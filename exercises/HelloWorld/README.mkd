
# HelloWorld ################################################################

(For more details of the development tools see [chapter two of the course
notes](https://iot.unphone.net/#sec:setup).)

A basic ESP32 example that prints (a few useful macro definitions) to serial
and blinks the inbuilt LED.

A minimal install, build, burn to device and then monitor would look like
this (plug in your ESP32 Feather first!):

```bash
magic.sh setup          # pull in the development tools if you don't have them
magic.sh                # compile, upload, monitor
```

For help see:

```bash
magic.sh -h
```

Files:

- `sketch`:             source code
  - `main.cpp`:         main entry points: `setup`, `loop`, `app_main`
  - `sketch.h`:         defines a macro for Arduino IDE builds

Generated files:

- `build`:              generated build files
