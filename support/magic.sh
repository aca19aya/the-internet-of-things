#!/bin/bash
# magic.sh
# scripts for configuring and building iot firmware for the ESP32 and unPhone
# hamish, autumn 2021 (seeded from dckrbldr.sh and old magic.sh)

# parameters ################################################################

# boilerplate locals
alias cd='builtin cd'
P="$(realpath -e $0)"
USAGE="`basename ${P}` [-h(elp)] [-d(ebug)] command-name [args]"
DBG=:
OPTIONSTRING=hdt:
R='\033[0;31m' # red (use with echo -e, or the e function below)
G='\033[0;32m' # green
B='\033[1;34m' # blue
Y='\033[0;93m' # yellow
M='\033[0;95m' # magenta
C='\033[0;96m' # cyan
N='\033[0m'    # no color

# specific locals
export TOOLING_DIR="$(cd `dirname ${P}` && pwd)/tooling"
export UNPHONE_DIR="$(cd `dirname ${P}`/../../unphone && pwd || :)"
export MKESP_DIR="${TOOLING_DIR}/makeEspArduino"
export MKESP_GIT="https://github.com/plerup/makeEspArduino.git"
export ARDIDE_VER="arduino-1.8.19"
export ARDIDE_DIR=${TOOLING_DIR}/${ARDIDE_VER}
export ARDIDE_PREFS_DIR=${TOOLING_DIR}/dot-arduino15
export ARDIDE_PREFS=${ARDIDE_PREFS_DIR}/preferences.txt
export ARDIDE_SKETCHBOOK_DIR=${TOOLING_DIR}/Arduino
export ARDIDE_BIN_DIR="build/ardide-bin-output"
export ARDUINO_ESPRESSIF_DIR=${ARDIDE_SKETCHBOOK_DIR}/hardware/espressif
export ARDUINO_ESP32_DIR=${ARDUINO_ESPRESSIF_DIR}/esp32
export ARDUINO_ESP32_GIT="https://github.com/espressif/arduino-esp32"
export ARDUINO_ESP32_VER="2.0.2"
export TIOT_GIT="https://gitlab.com/hamishcunningham/the-internet-of-things"
export UNPHONE_GIT="https://gitlab.com/hamishcunningham/unphone"
export IDF_VER="v4.4"
export IDF_GIT="https://github.com/espressif/esp-idf.git"


# boilerplate functions #####################################################

# message & exit if exit num present
e() { (
  c=""; case $1 in R|G|B|Y|M|C|N) c=${!1}; shift; ;; esac; echo -e "${c}$*${N}";
) }
usage() { e G "usage: $USAGE"; [ ! -z "$1" ] && exit $1; }

# process options
while getopts $OPTIONSTRING OPTION
do
  case $OPTION in
    h)	$P build-help; echo; usage 0 ;;
    d)	DBG=echo ;;
    t)	tag="${OPTARG}" ;;
    *)	usage 1 ;;
  esac
done 
shift `expr $OPTIND - 1`
ARGS=$*


# various utility functions #################################################

# local help
build-help() {
  e Y "$P commands available are:"
  tac $P >/tmp/$$
  for n in `grep -n '^[a-z0-9-]*() {' /tmp/$$ |sed 's,:.*$,,' |tac`
  do
    $DBG $n >&2
    nn=$((n+1))
    FNAME=`sed -n "${n}p" /tmp/$$ |sed 's,().*,,'`
    [ $FNAME == e -o $FNAME == usage -o $FNAME = colours ] && continue
    echo -en "$G${FNAME}: $N"
    sed -n "${nn},/^$/p" /tmp/$$ |sed 's,^#, ,' |tac
  done
}

# short hash of the current commit
git-hash() { git log --pretty=format:'%h' -n 1; }

# copy this firmware tree, modifying the name; $1 should have the new name
copy-me-to() {
  BASEDIR="$(cd `dirname ${P}` && pwd)"
  MYNAME=`basename $BASEDIR`
  PROJECT=$1
  PDIR=$PROJECT
  PNAME=`basename $PDIR`

  [ -e $PROJECT ] && { e R "oops: $PROJECT exists"; exit 1; }
  e B "creating project $PROJECT ..."

  COPYCANDIDATES=`ls -A`
  mkdir -p $PDIR
  for f in $COPYCANDIDATES
  do
    grep -qs "^${f}$" .gitignore || { cp -a ${f} $PDIR && e B $f copied; }
  done
  (
    cd $PDIR
    for f in `grep -sl ${MYNAME} * */*`
    do
      e B "sed $MYNAME to $PNAME in $f"
      sed -i "s,${MYNAME},$PNAME,g" $f
    done
    e G "$PNAME created at $PDIR"
  )
}

# run a web server in the build directory for firware OTA
ota-httpd() {
  # firmware versioning
  FIRMWARE_VERSION=$(grep 'int firmwareVersion =' main/main.cpp |
    sed -e 's,[^0-9]*,,' -e 's,;.*$,,')

  # create downloads for the httpd server, .bin and version
# TODO use the exported .bin name from docker or /tmp/mkesp
  cd build
  mkdir -p fwserver
  cd fwserver
  echo $FIRMWARE_VERSION >version
  [ -f Thing.bin ] || cp ../Thing.bin ${FIRMWARE_VERSION}.bin

  python -m http.server 8000
}

# scratchpad
scratch() {
  if false; then        # already done
    :
  elif true; then       # to do now
    :
  else                  # to do later
    :
  fi
}

# build with Espressif's ESP-IDF docker images (via idf.py); does things like:
#   docker run --device=/dev/ttyUSB0:/dev/ttyUSB0 -ti --rm -v \
#     $PWD:/project -w /project espressif/idf idf.py build
# results (e.g. build/, sdkconfig) will be exported to the current directory;
# options are passed to idf.py so use e.g.:
#   flash monitor         burn and listen (the default)
#   build                 just compile
#   set-target esp32s3    set a chip type
#   menuconfig            configure
#   monitor               listen on serial
idf() {
  ARGS=$*
  [ x"$*" == x ] && ARGS="flash monitor"
  docker run --device=/dev/ttyUSB0:/dev/ttyUSB0 -ti --rm -v \
    $PWD:/project -w /project espressif/idf idf.py $ARGS
}

# build with makeEspArduino.mk
# takes a command name and .ino as arguments (in that order);
# if not given, the command defaults to "run", which does build/flash/monitor;
# if not given, the sketch defaults to sketch/sketch.ino if it exists, or
# other .ino if found;
# unphone libs are included if unphone.h is #included from the source (you
# can force their or other lib inclusion via e.g. LIBS=~/unphone/lib ...)
mkesp() {
  CNAME=run
  SFILE=sketch/sketch.ino
  ARGS=""
  while :
  do
    [ -z "$1" ] && break
    case "$1" in
      *.ino) SFILE="$1" ;;
      *=*)   ARGS="$1 ${ARGS}" ;;
      *)     CNAME="$1" ;;
    esac
    shift
  done
  [ -f "${SFILE}" ] || { set `find . -name '*.ino'` ...; SFILE="$1"; }
  [ -f "${SFILE}" ] || { e R "can't find a .ino $SFILE, giving up!"; exit 1; }
  SKETCH="SKETCH=${SFILE}"
  SKETCH_DIR="`dirname ${SFILE}`"

  grep -q '^[      ]*#include[     ]*["<]unphone\.h' $SKETCH_DIR/* &&
    { e B adding unphone/lib to LIBS; LIBS="${UNPHONE_DIR}/lib ${LIBS}"; }

  COM="make -f ${MKESP_DIR}/makeEspArduino.mk ESP_ROOT=${ARDUINO_ESP32_DIR} \
    BOARD=featheresp32 CHIP=esp32 LIBS=${LIBS} ${SKETCH} ${ARGS} ${CNAME}"
  e Y "${COM}"
  eval ${COM}
}

# convert an old magic.sh build to a mkesp build by swapping the way main/ and
# sketch/ are linked (partly because mkesp's perl absolute path function
# dereferences links, so app_main.c won't compile as C, and partly because
# windoze Arduino IDE users can't open links from sketch/)
convert2mkesp() {
  [ -d main ]   || { e R "oops no main dir"; exit 1; }
  [ -d sketch ] || { e R "oops no sketch dir"; exit 1; }
  [ -L sketch/sketch.ino ] || {
    e R "oops, sketch.ino seems to be a file: has been converted already?"
    exit 1
  }
  cd sketch
  for f in *
  do
    [ -L $f ] || continue
    TARGET=$(readlink $f)
    rm $f
    mv $TARGET $f
    ( cd $(dirname $TARGET) && ln -s ../sketch/$f $(basename $TARGET) )
  done

  e Y "conversion changes made, check in?"
  cd ..
  e Y "sketch:"
  ls -l sketch
  e Y "main:"
  ls -l main
  git status .
}

# create a sketch suitable for mkesp (or ArdIDE) compilation from an IDF
# project; this is only a basic conversion for illustration purposes, and
# doesn't cope with extra components, or Kconfig, or etc.
idf2sketch() {
  [ -d sketch ] && { e R "oops, sketch already exists"; exit 1; }
  [ -d main ]   || { e R "oops, no main"; exit 2; }

  mkdir sketch
  cd sketch
  for f in ../main/*.h ../main/*.c
  do
    case $f in
      *main.c) continue ;;
      *) [ -f $f ] && ln -s $f ;;
    esac
  done
  sed -e 's,void app_main(,void loop(void) { }\nvoid setup(,' \
    ../main/*main.c >sketch.ino

  e G 'basics done; any config from main/Kconfig.projbuild also needed'
}

# clone the-internet-of-things and unphone repositories from gitlab
# (normally this should be done to $HOME)
clone() {
  e Y "clone the-internet-of-things & unphone repos to $PWD, ok? (y/n)"
  e Y "(normally this should be done to $HOME)"
  read ANS
  [ x$ANS == xy ] || { e Y "ok, giving up :)"; exit 1; }

  [ -d the-internet-of-things ] || git clone $TIOT_GIT
  [ -d unphone ] || git clone $UNPHONE_GIT
  e Y done
}

# download arduino core, IDF and makeEspArduino, and do get.py, and
# download and setup an Arduino IDE, all into ./tooling
setup() {
  e G "populating $TOOLING_DIR"

  [ -d $TOOLING_DIR ] || mkdir $TOOLING_DIR
  cd $TOOLING_DIR

  [ -d $ARDUINO_ESPRESSIF_DIR ] || (
    mkdir -p $ARDUINO_ESPRESSIF_DIR
    cd $ARDUINO_ESPRESSIF_DIR
    git clone $ARDUINO_ESP32_GIT esp32
    cd $ARDUINO_ESP32_DIR
    git checkout $ARDUINO_ESP32_VER
    cd tools
    python3 get.py
    groups |grep -q dialout || sudo usermod -aG dialout $USER
  )

  [ -d $MKESP_DIR ] || git clone $MKESP_GIT

  # install Arduino IDE
  [ -d $ARDIDE_VER ] ||
    (
      wget https://downloads.arduino.cc/${ARDIDE_VER}-linux64.tar.xz &&
      tar xJf ${ARDIDE_VER}-linux64.tar.xz &&
      rm ${ARDIDE_VER}-linux64.tar.xz
    )

  # create dot-arduino15 and set initial prefs
  [ -d $ARDIDE_PREFS_DIR ] || (
    mkdir $ARDIDE_PREFS_DIR &&
    echo "build.path=$ARDIDE_BIN_DIR" >> $ARDIDE_PREFS &&
    echo "sketchbook.path=$ARDIDE_SKETCHBOOK_DIR" >> $ARDIDE_PREFS &&
    echo "board=featheresp32" >> $ARDIDE_PREFS &&
    echo "update.check=false" >> $ARDIDE_PREFS &&
    echo "custom_DebugLevel=featheresp32_none" >> $ARDIDE_PREFS &&
    echo "custom_FlashFreq=featheresp32_80" >> $ARDIDE_PREFS &&
    echo "custom_UploadSpeed=featheresp32_921600" >> $ARDIDE_PREFS &&
    echo "serial.debug_rate=115200" >> $ARDIDE_PREFS &&
    echo "target_package=espressif" >> $ARDIDE_PREFS &&
    echo "target_platform=esp32" >> $ARDIDE_PREFS &&
    :
  );

  e Y "arduino-esp32 dir is $(ls -ld $ARDUINO_ESP32_DIR)"
  e Y "mkesp dir is $(ls -ld $MKESP_DIR)"
  e Y "prefs dir is $(ls -ld $ARDIDE_PREFS_DIR)"
  e Y "sketch dir is $(ls -ld $ARDIDE_SKETCHBOOK_DIR)"

  e G "$(basename $TOOLING_DIR) done"
}

# clone a version of ESP-IDF into tooling
clone-idf() {
  [ -d $TOOLING_DIR ] || mkdir $TOOLING_DIR
  cd $TOOLING_DIR

  e Y "git clone -b \"${IDF_VER}\" --recursive \"${IDF_GIT}\""
  git clone -b "${IDF_VER}" --recursive "${IDF_GIT}"
}

# run the Arduino IDE, using preferences and Arduino/ from ./tooling
ardide() {
  e G "running ${ARDIDE_DIR}/arduino --preferences-file $ARDIDE_PREFS $*"
  ${ARDIDE_DIR}/arduino --preferences-file $ARDIDE_PREFS $*
}

# run build commands (e.g. mkesp or pio) in a docker image.
# if used with no arguments defaults to mkesp in the current (mapped) dir;
# if supplying arguments, they need both single and double quotes, e.g.:
#   magic.sh dckr '"cd project; pio run -t upload -t monitor"'
dckr() {
  COM="$*"
  [ -z "$COM" ] && \
    COM="cd project && ../the-internet-of-things/support/magic.sh mkesp"
  docker run -ti -v $PWD:/home/ubuntu/project --device /dev/ttyUSB0 \
    hamishcunningham/iot:pio /bin/bash -ic "$COM"
}


# command runner ############################################################

# the main event... run commands
if [ -z "$*" ]
then
  usage
  exit 0
else
  COMMAND="$*"
fi
e Y running $COMMAND ...
eval $COMMAND
exit $?
